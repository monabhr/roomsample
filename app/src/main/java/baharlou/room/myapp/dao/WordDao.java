package baharlou.room.myapp.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import baharlou.room.myapp.classes.Word;

@Dao
public interface WordDao {

    @Insert
    void insert(Word word);

    @Query("Delete from word_table")
    void deleteAll();

    @Query("Select * from word_table")
    LiveData<List<Word>> getAll();


  /*
    @Insert(onConflict = OnConflictStrategy.REPLACE) */
    /*@Delete
    void delete(Word word);

    @Update
    void update(Word word);*/

}
