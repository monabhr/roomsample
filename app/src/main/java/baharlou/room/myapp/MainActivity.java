package baharlou.room.myapp;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import baharlou.room.myapp.adapter.WordListAdapter;
import baharlou.room.myapp.classes.Word;
import baharlou.room.myapp.viewmodel.WordViewModel;

public class MainActivity extends AppCompatActivity {
    //Android Architecture Components
    //https://www.raywenderlich.com/6729-android-jetpack-architecture-components-getting-started
    //https://codelabs.developers.google.com/codelabs/android-databinding/#0
    //https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#11

    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;

    private WordViewModel wordViewModel;
    WordListAdapter adapter;
    FloatingActionButton fab;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();
        fillRecyclerView();
        bindViewModelToUI();


    }

    private void bindViews() {

        recyclerView = findViewById(R.id.recyclerview);
        fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
                startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
            }
        });
    }

    private void bindViewModelToUI() {

        wordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);

        wordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(@Nullable List<Word> words) {

                if (words != null && adapter != null)
                    adapter.setWords(words);
            }
        });
    }

    private void fillRecyclerView() {
        adapter = new WordListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Word word = new Word(data.getStringExtra(NewWordActivity.EXTRA_REPLY));
            wordViewModel.insert(word);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }

    }
}
