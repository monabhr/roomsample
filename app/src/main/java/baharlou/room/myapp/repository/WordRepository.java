package baharlou.room.myapp.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

import baharlou.room.myapp.classes.Word;
import baharlou.room.myapp.dao.WordDao;
import baharlou.room.myapp.room.WordRoomDatabase;

/**
 * Why use a Repository?
 * <p>
 * A Repository manages query threads
 * and allows you to use multiple backends.
 * In the most common example,
 * the Repository implements
 * the logic for deciding whether to fetch data from a network
 * or use results cached in a local database.
 */
public class WordRepository {

    //Add member variables for the DAO and the list of words
    private WordDao wordDao;
    private LiveData<List<Word>> allWords;

    //Add a constructor that gets a handle to the database
    // and initializes the member variables.
    public WordRepository(Application application) {

        WordRoomDatabase roomDatabase = WordRoomDatabase.getDatabase(
                application
        );

        wordDao = roomDatabase.wordDao();
        allWords = roomDatabase.wordDao().getAll();
    }

    //Observed LiveData will notify the observer when the data has changed
    public LiveData<List<Word>> getAllWords() {
        return allWords;
    }

    //You must call this on a non-UI thread or your app will crash
    // Room ensures that you don't do any long-running operations on the main thread, blocking the UI.
    public void insert(Word word) {
        new insertAsyncTask(wordDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Word, Void, Void> {

        private WordDao mAsyncTaskDao;

        insertAsyncTask(WordDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Word... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
