package baharlou.room.myapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import java.util.List;

import baharlou.room.myapp.classes.Word;
import baharlou.room.myapp.repository.WordRepository;

/***
 * Never pass context into ViewModel instances.
 * Do not store Activity, Fragment, or View instances or their Context in the ViewModel
 * For example, an Activity can be destroyed and created many times during the lifecycle of a ViewModel as the device is rotated. If you store a reference to the Activity in the ViewModel, you end up with references that point to the destroyed Activity. This is a memory leak
 * If you need the application context, use AndroidViewModel
 *
 * ViewModel is not a replacement for the onSaveInstanceState() method, because the ViewModel does not survive a process shutdown
 */
public class WordViewModel extends AndroidViewModel {

    private WordRepository wordRepository;

    private LiveData<List<Word>> allWords;


    public WordViewModel(@NonNull Application application) {
        super(application);

        wordRepository = new WordRepository(application);
        allWords = wordRepository.getAllWords();
    }

    public LiveData<List<Word>> getAllWords() {
        return allWords;
    }

    public void insert(Word word) {
        wordRepository.insert(word);
    }

}
